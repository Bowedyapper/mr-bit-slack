export default [
    {
        "name": "1.1.1.1",
        "type": "Agile",
        "group": "Principles of Programming",
        "descriptor": "Explain the importance of delivering value early and often, iterating and continuously improving workflows where necessary.",
        "explanation": "For this learning outcome you need to demonstrate through your work how and why you have delivered the functionality in the order you have.",
        "example": "If you are given a project in which you need to write tests for a piece of functionality, you need to have written the tests and reflected on the order you did them and how this benefitted the project as a whole."
    },
    {
        "name": "1.1.1.2",
        "type": "Business",
        "group": "Principles of Programming",
        "descriptor": "Explain and apply in an individual context basic business behaviour, ethics and courtesies, demonstrating timeliness and focus when faced with distractions and the ability to complete tasks to a deadline with high quality.",
        "explanation": "For this learning outcome you need to explain how you have applied business related practices, such as timeliness and focus during a project.",
        "example": "You might reflect on how you managed the conflict between your project work and completing a set of learning resources. You might describe how you split you day into learning and practice. You might screenshot a timetable you have developed for yourself to ensure you deliver on time."
    },
    {
        "name": "1.1.1.3",
        "type": "Employability",
        "group": "Principles of Programming",
        "descriptor": "Discuss how the team demonstrated the following capabilities: good communication, identifying different abilities and potential, and showing respect for individuals.",
        "explanation": "For this learning outcome you need to collect evidence of how you communicated effectively with your team.",
        "example": "Evidence for this learning outcome might come from feedback from peers on how you handled criticism during a code review or from an observation by a member of the delivery staff."
    },
    {
        "name": "1.1.1.4",
        "type": "Security",
        "group": "Principles of Programming",
        "descriptor": "Describe the nature of risk to information and information systems and define what cyber security is, and explain its importance when developing software solutions and mitigating risk.",
        "explanation": "For this learning outcome you need to demonstrate by your practical work and reflections that you understand what cyber security is and how it affects your programming decisions.",
        "example": "In your project you might record a short video explaining the possibilities for attacking the piece of software you are working. Explaining how the software protects login credentials and anonymises certain pieces of information."
    },
    {
        "name": "1.1.1.5",
        "type": "Technical",
        "group": "Principles of Programming",
        "descriptor": "Write programs in a variety of languages that can use common features for control flow and data structures.",
        "explanation": "For this learning outcome you will need to evidence that you apply common programming techniques to achieve a goal.",
        "example": "For instance you might write a test where you create a list of items and feed this to a class and test the output."
    },
    {
        "name": "1.1.2.1",
        "type": "Agile",
        "group": "Applied algorithms and data structures",
        "descriptor": "Identify and manage deviations from the planned schedule of a project",
        "explanation": "For this learning outcome you need to demonstrate that your team could identify when they were not in line with their planned schedule and explain how this deviation was managed.",
        "example": "An example of evidence for this would be the original project kanban and then an adjusted one. You would add a reflection on what changed, why and how this helped you get achieve your goals."
    },
    {
        "name": "1.1.2.2",
        "type": "Business",
        "group": "Applied algorithms and data structures",
        "descriptor": "Explain the software development process as aligned to industry practice.",
        "explanation": "For this learning outcome you need to evidence how the processes you are using during your projects aligns with that followed in industry. In the most part this refers to how Agile is used in an industry context.",
        "example": "Evidence of this might be a reflection based on a guest speaker talking about how Agile is used in their business and the similiarities to the way your team has been working. It could also be a video with your team discussing the similarities with the way you have been working and recent placements or conference talk you have watched."
    },
    {
        "name": "1.1.2.3",
        "type": "Employability",
        "group": "Applied algorithms and data structures",
        "descriptor": "Produce and receive feedback, constructively applying appropriate techniques and incorporate it into his/her own development and life-long learning.",
        "explanation": "For this learning outcome you need to demonstrate that you can produce feedback for yourself to enhance your development and learning. You also need to be able to provide feedback to others to help them develop their skills.",
        "example": "Evidence for this might come from a code review, an observation you have done with a peer, or a reflection based on some written feedback."
    },
    {
        "name": "1.1.2.4",
        "type": "Security",
        "group": "Applied algorithms and data structures",
        "descriptor": "Explain why cyber security matters – the importance to business and society, which includes the inclusion and significance of cyber security to critical and safety critical systems (including healthcare systems, critical national infrastructure, industrial plant automation, autonomous vehicles, mass transportation, internet of things).",
        "explanation": "For this learning outcome you need to demonstrate an awareness of cyber security matters in the projects you are engaged in. When engaging in a project you should be thinking about the context in which the product is to be used and any associated security matters.",
        "example": "Evidence for this might be a video of the team discussing security context of the product, or it might be screenshots of recent CVE reports on similar software and why these might be of interest."
    },
    {
        "name": "1.1.2.5",
        "type": "Technical",
        "group": "Applied algorithms and data structures",
        "descriptor": "Describe algorithms an abstract level, and compare the tradeoff between memory and performance.",
        "explanation": "For this learning outcome you are expected to demonstrate that you understand some common computer algorithsm such as arrays, lists and trees. You will also be required to demonstrate that you understand how to compare algorithms in terms of the size of their inputs.",
        "example": "Evidence for this learning outcome might take the form of code to a repository where you implement one or more data structures and algorithms. You might also reflect on the performance of the application you are working on and how it could be improved with the use of a different type of data structure."
    },
    {
        "name": "1.1.3.1",
        "type": "Agile",
        "group": "Creativity through patterns",
        "descriptor": "Discuss the importance of regular project reviews, and engage with the project review process.",
        "explanation": "For this learning outcome you need to evidence that you have held regular project reviews and engaged with the project review process.",
        "example": "Evidence for this might include videos highlighting parts of recorded project reviews and reflections about how these meetings helped the project progress and the team coordinate their actions."
    },
    {
        "name": "1.1.3.2",
        "type": "Business",
        "group": "Creativity through patterns",
        "descriptor": "Explain what a threat model is in a business context, which includes documenting what information is at risk, analysing the type and level of risk realised; and the impact of the risk being realised.",
        "explanation": "For this learning outcome you must be able to demonstrate that you can idenfity what a threat model is, document the threats including level and impact of risk in a business context.",
        "example": "An example of evidence for this learning outcome might be a document the team has created for a project identifying the threats and a reflection on what you perceive as the benefits of drawing up such a document."
    },
    {
        "name": "1.1.3.3",
        "type": "Employability",
        "group": "Creativity through patterns",
        "descriptor": "Examine the purpose, audience and the outcomes to be achieved when communicating with stakeholders, and decide which method of communication to use and the level of formality required.",
        "explanation": "For this learning outcome you need to demonstrate that when doing a presentation you can consider a range of factors to ensure the presentation is appropriate. This presentation might be done for a project you are working on or an external talk for instance at a coding club.",
        "example": "Example evidence of this might be a recording with you presenting some information to your team, or it might a reflection on what you think would be the best way to present some information to the original development team, perhaps via a Git push request in an Open Source project."
    },
    {
        "name": "1.1.3.4",
        "type": "Security",
        "group": "Creativity through patterns",
        "descriptor": "Identify and apply industry standard static and dynamic code analysis frameworks (e.g FindBugs, Checkstyle, etc).",
        "explanation": "Demonstrate you can apply common static and dynamic code analysis techniques to a project you are working on.",
        "example": "Evidence of this might be generating associated reports or recording a screencast of you carrying out such techniques."
    },
    {
        "name": "1.1.3.5",
        "type": "Technical",
        "group": "Creativity through patterns",
        "descriptor": "Use pseudocode or programming language to implement, test and debug algorithms for manipulating data in a variety of computational settings.",
        "explanation": "For this learning outcome you need to demonstrate your coding skills including use of testing, algorithms and coding patterns in a range of project settings.",
        "example": "Evidence for this learning outcome should come from commits to either your team or personal repository for a range of projects. You can also write reflections on why you implemented your code in the way that you did."
    },
    {
        "name": "1.2.1.1",
        "type": "Agile",
        "group": "Continuous development",
        "descriptor": "Describe the software development lifecycles and processes.",
        "explanation": "Provide evidence that you understand the software development lifecyle that you have been working through and evidence how you have been using the processes to provide structure to your working week.",
        "example": "An example of evidence for this learning outcome might be a screenshot of the kanban board for the team, and a reflection on how it has helped (or hindered) your progress during a sprint."
    },
    {
        "name": "1.2.1.2",
        "type": "Business",
        "group": "Continuous development",
        "descriptor": "Explain that information is an organisational asset that has utility and a value which may be relative depending on the perspective taken, and examine it’s attributes relating to confidentiality, possession, integrity, authenticity and availability.",
        "explanation": "For this learning outcome you need to provide evidence that you understand and can explain how information should be treated and valued within an organisation. This might be through analysing how information is used in a project you are doing or in resources that you have been reading about.",
        "example": "An example of evidence that could be used towards this learning outcome is a video describing how information for a CRM, that you are using as a project, might be protected and access to the data limited. Evidence might also come from a reflection on articles you have read about the best practices between using live data when testing software."
    },
    {
        "name": "1.2.1.3",
        "type": "Employability",
        "group": "Continuous development",
        "descriptor": "Produce, implement and review a personal development plan.",
        "explanation": "For this learning outcome you need to complete a personal development plan and refer to it in a reflection at the end of each semester.",
        "example": "The evidence for this is a completed personal development plan outlining your goals and how intend to achieve them. You might write a reflective piece at the end of each semester on how you have done in progressing towards your goals and if they have changed throughout the year."
    },
    {
        "name": "1.2.1.4",
        "type": "Security",
        "group": "Continuous development",
        "descriptor": "Compare and contrast the different methods and techniques used to assure the quality of a software development processes and deliverables.",
        "explanation": "For this learning outcome you need to evidence that you understand the different methods that you can use to ensure that you are developing quality software.",
        "example": "An example of evidence for this might be a short video or screencast of you writing tests for a piece of software and you explaining why you are writing them in a certain style."
    },
    {
        "name": "1.2.1.5",
        "type": "Technical",
        "group": "Continuous development",
        "descriptor": "Select, with justification, and apply an appropriate set of tools to support the development of a range of software products of medium size.",
        "explanation": "For this learning outcome you need to submit evidence that demonstrates you can setup and apply common tools for development. This might include version control, continuous testing tools and documentation.",
        "example": "Example of evidence for this might be a docker and a jenkins definition file. It might also include a short reflection on what tools you used and the benefits these made to your development process."
    },
    {
        "name": "1.3.1.1",
        "type": "Agile",
        "group": "Databases",
        "descriptor": "Examine the roles and responsibilities of a typical agile project management team and discuss how they interact.",
        "explanation": "This learning outcome expects you learn about the different parts of a agile team and what they do on a day to day basis. As part of your team activity you should be trying to emulate these roles and to get experience of why they are important.",
        "example": "Evidence you can gather for this might be reflection on the performance of the team based on the roles and responsibilities the team chose. You might also video the team discussing how they could have improved the role definitions at the end of a sprint."
    },
    {
        "name": "1.3.1.2",
        "type": "Business",
        "group": "Databases",
        "descriptor": "Show how a business could be vulnerable to threats from systems and people.",
        "explanation": "This learning outcome allows you to consider how a business might be vulnerable to attacks from other systems or from internal or external employees. You should be considering topics such as social engineering and viruses. You can place in the context of a business that might be running the software you are using as part of your project. As this is part of the databases module you might consider how a business's data is under threat from manipulation or deletion by attackers.",
        "example": "Evidence for this might be a reflection, citing news and academic literature, highlighting particular attacks. Also you might provide evidence of consideration of a variety of threats by adding a threat analysis to your repository to refer to."
    },
    {
        "name": "1.3.1.3",
        "type": "Employability",
        "group": "Databases",
        "descriptor": "Describe, with reference to different learning strategies, how life-long learning is important for a technical career and development in a corporate environment.",
        "explanation": "This learning outcome asks you to consider the way you learn and how you can ensure that you are continuously keeping up to date and developing your technical and soft skills.",
        "example": "Examples of evidence for this might be part of your Personal Development Plan, or identifying strengths and weaknesses in how you learn in a reflection."
    },
    {
        "name": "1.3.1.4",
        "type": "Security",
        "group": "Databases",
        "descriptor": "Explain what is meant by the term “insider”, examining the security implications on an organisation’s network, system and data.",
        "explanation": "This learning outcome is looking for you to understand how insiders can be a threat to an organisations IT infrastructure. For isntance, what if a developer left in a backdoor into the organisation, or a timebomb to delete key information after they have left.",
        "example": "Evidence for this learning outcome might be video or written reflection on how you can check your code is authentic. As a team you might try to write a report that highlights how a system could be checked for vulnerabilities by a malicious insider."
    },
    {
        "name": "1.3.1.5",
        "type": "Technical",
        "group": "Databases",
        "descriptor": "Explain the basic principles of a relational database and produce a relational database that incorporates key, entity integrity, and referential integrity constraints.",
        "explanation": "This learning outcome looks at how data is structured in a persistent system such as a relational database. You will be expected to be able to demonstrate how to access and search for data.",
        "example": "Evidence for this learning outcome could be code committed to a repository for creating a particular database structure. It could also be a reflection on a particular query and why it might be not as efficient as one you propose."
    },
    {
        "name": "1.3.2.1",
        "type": "Agile",
        "group": "Data Languages",
        "descriptor": "Explain and illustrate the issues of quality, cost and time concerned with project implementation, including contractual obligations and resource constraints.",
        "explanation": "For this learning outcome you are asked to consider the common qualities that are tracked with a project. You should consider potential contractual requirements and handling resource constraints. These should be reflected in the way you organise your tasks throughout the sprint.",
        "example": "Example evidence might be a kanban board or burndown charts. You might reflect on how resource constraints such as time and budget have impacted your ability to complete the work."
    },
    {
        "name": "1.3.2.2",
        "type": "Business",
        "group": "Data Languages",
        "descriptor": "Describe the importance of time management and be able to demonstrate competence in its application.",
        "explanation": "This learning outcome is about how you structure your time to achieve your goals. This can be considered in terms of personal learning goals or team goals. Your Personal Development Plan and team sprints should provide evidence for how you currently structure and how you could structure your time to maximise productivity.",
        "example": "Example evidence for this include reflections and kanban boards showing time management techniques. You should try and to show progress over time and highlight the decisions made by both you and the wider team to manage time."
    },
    {
        "name": "1.3.2.3",
        "type": "Employability",
        "group": "Data Languages",
        "descriptor": "Show how teams work effectively to produce technology solutions, working with team members to identify and solve problems and disagreements, sharing feedback with others on the achievement of team objectives and making suggestions and encouragement for improving team-working.",
        "explanation": "This learning outcome asks you to consider what makes a good team and how a team can best collaborate towards common objectives.",
        "example": "Evidence for this learning outcome might be a reflection by the team on its performance. It might refer to recorded meetings, conflicts and reconciliations about key decisions."
    },
    {
        "name": "1.3.2.4",
        "type": "Security",
        "group": "Data Languages",
        "descriptor": "Analyse the processes and controls that need to be implemented to maintain the required level of security of a component, product, or system through its lifecycle and at end of life.",
        "explanation": "This learning outcome asks you to consider the appropriate level of security around key parts of a system through its lifecycle. You might look at the database, data between client and host, or some other part of the system. Analyse the threats and mitigations that you could take to minimise the threats.",
        "example": "Evidence might include a threat analysis of the system and reflections on recent security breaches that are similar."
    },
    {
        "name": "1.3.2.5",
        "type": "Technical",
        "group": "Data Languages",
        "descriptor": "Write stored procedures that deal with parameters and have some control flow to provide given functionality.",
        "explanation": "This learning outcome looks how to interact with a data course. In particualr it looks at using stored procedures and control flow to provide functionality within a database setting.",
        "example": "Examples of evidence include committing relevant code to the repository. It might also be a reflection on discussing if a particular piece of functionality might benefit from being a stored procedure."
    },







    {
        "name": "2.4.1.1",
        "type": "Agile",
        "group": "Multiplatform development",
        "descriptor": "Explain and compare project management environments, such as 'waterfall' methods, and be able to apply the underpinning philosophy and principles of Agile in a project situation even in a non-agile environment."
    },
    {
        "name": "2.4.1.2",
        "type": "Business",
        "group": "Multiplatform development",
        "descriptor": "Assess, in a team context, basic business behaviour, ethics and courtesies, demonstrating timeliness and focus when faced with distractions and the ability to complete tasks to a deadline with high quality."
    },
    {
        "name": "2.4.1.3",
        "type": "Employability",
        "group": "Multiplatform development",
        "descriptor": "Make concise, engaging and well-structured verbal presentations and explanations of varying lengths, with and without the use of media, always taking into account the audience viewpoint."
    },
    {
        "name": "2.4.1.4",
        "type": "Security",
        "group": "Multiplatform development",
        "descriptor": "Correctly apply the organisation’s security architecture to any particular systems or solutions development activities, and rewrite a simple program to remove common vulnerabilities, such as buffer overflows, integer overflows, and race conditions."
    },
    {
        "name": "2.4.1.5",
        "type": "Technical",
        "group": "Multiplatform development",
        "descriptor": "Create appropriate implementations for a selection of different contexts for HCI (mobile devices, consumer devices, business applications, web, business applications, collaboration systems, games, etc.) and be able to produce a user-centred design that explicitly recognises the user and is DDA compliant (Disability Discrimination Act)."
    },
    {
        "name": "2.4.2.1",
        "type": "Agile",
        "group": "Software services",
        "descriptor": "Explain agile project delivery and show how it applies to the roll out of a software project, ensuring an accurate and timely deployment in a customer friendly way, consistent with the customer needs."
    },
    {
        "name": "2.4.2.2",
        "type": "Business",
        "group": "Software services",
        "descriptor": "Compare, from a client-side perspective, threat, risk and vulnerability, and analyse typical threats, attacks and exploits."
    },
    {
        "name": "2.4.2.3",
        "type": "Employability",
        "group": "Software services",
        "descriptor": "Be creative, self-motivated, self-aware and able to reflect on successes and failures in ways that strengthen positive attitude and develop self-reliance through an understanding of their own personal preferences, styles, strengths and weaknesses."
    },
    {
        "name": "2.4.2.4",
        "type": "Security",
        "group": "Software services",
        "descriptor": "Appraise and manage risk for threats and vulnerabilities to information systems on an ongoing basis."
    },
    {
        "name": "2.4.2.5",
        "type": "Technical",
        "group": "Software services",
        "descriptor": "Explain and apply concepts such as loose coupling, separation of concerns, gang of four, multi-tiered architectures."
    },
    {
        "name": "2.5.1.1",
        "type": "Agile",
        "group": "Software architecture",
        "descriptor": "Analyse a range of digital development activities and estimate the costs and effort for allocating and managing appropriate phased contingency."
    },
    {
        "name": "2.5.1.2",
        "type": "Business",
        "group": "Software architecture",
        "descriptor": "Compare, from a service providers perspective, threat, risk and vulnerability, and analyse typical threats, attacks and exploits."
    },
    {
        "name": "2.5.1.3",
        "type": "Employability",
        "group": "Software architecture",
        "descriptor": "Apply analytical and critical thinking skills to Technology Solutions development and to systematically analyse and apply structured problem-solving techniques to them."
    },
    {
        "name": "2.5.1.4",
        "type": "Security",
        "group": "Software architecture",
        "descriptor": "Describe the types of testing that are commonly applied to identify vulnerabilities in software and show how to make software more resilient to threats."
    },
    {
        "name": "2.5.1.5",
        "type": "Technical",
        "group": "Software architecture",
        "descriptor": "Examine the fundamental components of technology solutions in a range of typical modern business environments, explain their interactions for any applicable target system (e.g. games console, smart-phone, embedded system) and compare the benefits and the differences of these environments."
    },
    {
        "name": "2.5.2.1",
        "type": "Agile",
        "group": "Software Quality Assurance",
        "descriptor": "Analyse the importance for the need to identify and manage project deliverables, identifying benefits for development and customer."
    },
    {
        "name": "2.5.2.2",
        "type": "Business",
        "group": "Software Quality Assurance",
        "descriptor": "Compare and contrast different industry standard software development processes, including distributed work (e.g. onshore, near shore and offshore), and identify the key enablers to make each model successful."
    },
    {
        "name": "2.5.2.3",
        "type": "Employability",
        "group": "Software Quality Assurance",
        "descriptor": "Examine the use of performance evaluation tools in a business context, and justify how his/her own work is providing value to the team."
    },
    {
        "name": "2.5.2.4",
        "type": "Security",
        "group": "Software Quality Assurance",
        "descriptor": "Implement robust, scalable and future-proof software security solutions that meet specific and generic requirements, and internal/external security standards and best practice."
    },
    {
        "name": "2.5.2.5",
        "type": "Technical",
        "group": "Software Quality Assurance",
        "descriptor": "Compare and contrast the different types and levels of verification (analysis, demonstration, test, formal proof, inspection etc.) and testing (unit, integration, systems, and acceptance) including the role and value of test driven development techniques."
    },
    {
        "name": "2.6.1.1",
        "type": "Agile",
        "group": "Data transformation",
        "descriptor": "Apply industry standard process, methods, techniques and tools to execute projects."
    },
    {
        "name": "2.6.1.2",
        "type": "Business",
        "group": "Data transformation",
        "descriptor": "Differentiate between features and benefits, giving appropriate examples, and show how to use these effectively in a marketing campaign"
    },
    {
        "name": "2.6.1.3",
        "type": "Employability",
        "group": "Data transformation",
        "descriptor": "Explain what is active listening. Demonstrate how active listening can be applied in appreciating others view and contributions."
    },
    {
        "name": "2.6.1.4",
        "type": "Security",
        "group": "Data transformation",
        "descriptor": "Examine the technical aspects of information security including client data protection and the data protection act."
    },
    {
        "name": "2.6.1.5",
        "type": "Technical",
        "group": "Data transformation",
        "descriptor": "Explain the role of data mining and the algorithms developed to address different data mining goals, and relate the application of these algorithms to real-world problems including big data."
    },
    {
        "name": "2.6.2.1",
        "type": "Agile",
        "group": "Data as a service",
        "descriptor": "Evaluate the quality assurance processes that can be automated during software development to achieve and maintain project requirements."
    },
    {
        "name": "2.6.2.2",
        "type": "Business",
        "group": "Data as a service",
        "descriptor": "Examine human aspects of information security including client data protection, GDPR and the data protection act."
    },
    {
        "name": "2.6.2.3",
        "type": "Employability",
        "group": "Data as a service",
        "descriptor": "With guidance, conduct research, using common industry literature and other media, into IT and business-related topics."
    },
    {
        "name": "2.6.2.4",
        "type": "Security",
        "group": "Data as a service",
        "descriptor": "Analyse the common pitfalls and mitigations of common security approaches."
    },
    {
        "name": "2.6.2.5",
        "type": "Technical",
        "group": "Data as a service",
        "descriptor": "Implement a data driven website, explaining the relevant technologies involved in each tier of the architecture and the accompanying performance trade-offs."
    },







    {
        "name": "3.7.1.1",
        "type": "Agile",
        "group": "Industrial software design",
        "descriptor": "Follow a systematic methodology for initiating, planning, executing, controlling, and closing a technology solutions project."
    },
    {
        "name": "3.7.1.2",
        "type": "Business",
        "group": "Industrial software design",
        "descriptor": "Evaluate the strategic importance of business processes and demonstrate an ability to document and understand them."
    },
    {
        "name": "3.7.1.3",
        "type": "Employability",
        "group": "Industrial software design",
        "descriptor": "Effectively prepare and deliver presentations using relevant presentation media products, present ideas clearly and convincingly."
    },
    {
        "name": "3.7.1.4",
        "type": "Security",
        "group": "Industrial software design",
        "descriptor": "Examine and apply standards for software quality planning and assurance (e.g. ISO/IEC 9126 - international standard for the evaluation of software quality)."
    },
    {
        "name": "3.7.1.5",
        "type": "Technical",
        "group": "Industrial software design",
        "descriptor": "Compare and contrast software design and development methodologies (e.g., structured or object-oriented), and be able to apply appropriate industry standard design notation such as UML and agile user story management."
    },
    {
        "name": "3.7.2.1",
        "type": "Agile",
        "group": "User experience design",
        "descriptor": "Justify project scope, timescale, aims and objectives in the construction of a project specification."
    },
    {
        "name": "3.7.2.2",
        "type": "Business",
        "group": "User experience design",
        "descriptor": "Investigate a company’s basic business functions, organisational structure and the roles of its senior leadership team."
    },
    {
        "name": "3.7.2.3",
        "type": "Employability",
        "group": "User experience design",
        "descriptor": "Conduct effective research, using literature and other media, into IT and business-related topics."
    },
    {
        "name": "3.7.2.4",
        "type": "Security",
        "group": "User experience design",
        "descriptor": "Investigate the key steps in managing security incidents and how to apply them."
    },
    {
        "name": "3.7.2.5",
        "type": "Technical",
        "group": "User experience design",
        "descriptor": "Assess a problem domain to establish a basis for the creation of a software design and show what the requirements of the customer are through solution design modelling."
    },
    {
        "name": "3.7.3.1",
        "type": "Agile",
        "group": "Releasing a minimum viable product",
        "descriptor": "Evaluate and apply software design and development methodology (e.g., structured or object-oriented), and be able to create appropriate industry standard design notation such as UML and agile user story management."
    },
    {
        "name": "3.7.3.2",
        "type": "Business",
        "group": "Releasing a minimum viable product",
        "descriptor": "Manage basic considerations: prioritisation, task versus responsibility management."
    },
    {
        "name": "3.7.3.3",
        "type": "Employability",
        "group": "Releasing a minimum viable product",
        "descriptor": "Plan and implement work goals, objectives, priorities and responsibilities with others."
    },
    {
        "name": "3.7.3.4",
        "type": "Security",
        "group": "Releasing a minimum viable product",
        "descriptor": "Apply recognised principles to the building of high-quality software components, understanding the difference between safety and quality, and show how mechanisms that improve quality may be used to partially underpin a safety argument."
    },
    {
        "name": "3.7.3.5",
        "type": "Technical",
        "group": "Releasing a minimum viable product",
        "descriptor": "Confidently design and apply algorithms for manipulating data in programming solutions for a variety of problems."
    },
    {
        "name": "3.8.1.1",
        "type": "Agile",
        "group": "Software release management",
        "descriptor": "Investigate the principles of quality assurance for project deliverables, including contractual obligations."
    },
    {
        "name": "3.8.1.2",
        "type": "Business",
        "group": "Software release management",
        "descriptor": "Explain what is meant by managing up and across the organisation, assessing people considerations."
    },
    {
        "name": "3.8.1.3",
        "type": "Employability",
        "group": "Software release management",
        "descriptor": "Be able to appraise, present a case for, and gain commitment to, a moderately complex technology-orientated solution, demonstrating understanding of business need, using open questions and summarising skills and basic negotiating skills."
    },
    {
        "name": "3.8.1.4",
        "type": "Security",
        "group": "Software release management",
        "descriptor": "Evaluate software processes for governing software development both technically, and in terms of cost control, quality, adherence of technical strategy and IPR identification."
    },
    {
        "name": "3.8.1.5",
        "type": "Technical",
        "group": "Software release management",
        "descriptor": "Determine configuration management processes for use throughout the product development life cycle in storing software deliverables and controlling and tracking changes to software both at component and release level, using configuration management tools effectively, and apply change management processes properly."
    },
    {
        "name": "3.9.1.1",
        "type": "Agile",
        "group": "Insight from data",
        "descriptor": "Determine the nature of risk and describe how to respond to risks."
    },
    {
        "name": "3.9.1.2",
        "type": "Business",
        "group": "Insight from data",
        "descriptor": "Evaluate strategic importance of data analytics for business decision making."
    },
    {
        "name": "3.9.1.3",
        "type": "Employability",
        "group": "Insight from data",
        "descriptor": "Demonstrate competence in gathering and evaluating information from people using a variety of techniques including interviewing."
    },
    {
        "name": "3.9.1.4",
        "type": "Security",
        "group": "Insight from data",
        "descriptor": "Maintain and manage software and hardware patching, particularly implementing vulnerability patching where vulnerabilities are identified."
    },
    {
        "name": "3.9.1.5",
        "type": "Technical",
        "group": "Insight from data",
        "descriptor": "Design, develop and evaluate a non-trivial big data analysis system."
    },
    {
        "name": "3.9.2.1",
        "type": "Agile",
        "group": "Data led decision making",
        "descriptor": "Formulate a plan to communicate risks through risk reports, registers or logs."
    },
    {
        "name": "3.9.2.2",
        "type": "Business",
        "group": "Data led decision making",
        "descriptor": "Describe employment relations issues and evaluate the application of policies and procedures for managing these relationships, and the application of good practice relating to equality and diversity issues in the workplace."
    },
    {
        "name": "3.9.2.3",
        "type": "Employability",
        "group": "Data led decision making",
        "descriptor": "Evaluate the motivations of internal and external stakeholders with respect to competing interests and objectives."
    },
    {
        "name": "3.9.2.4",
        "type": "Security",
        "group": "Data led decision making",
        "descriptor": "Determine residual risk and what the impacts are for an organisation."
    },
    {
        "name": "3.9.2.5",
        "type": "Technical",
        "group": "Data led decision making",
        "descriptor": "Report on the requirements development process: elicitation, specification, analysis and management and the use of tools for managing requirements in the context of data."
    },








    {
        "name": "4.10.1.1",
        "type": "Agile",
        "group": "MVP - Software challenge",
        "descriptor": "Create a requirements document that agrees, with respect to all stakeholders involved, the purpose of the project."
    },
    {
        "name": "4.10.1.2",
        "type": "Business",
        "group": "MVP - Software challenge",
        "descriptor": "Create a business plan for a product, including a cashflow forecast."
    },
    {
        "name": "4.10.1.3",
        "type": "Employability",
        "group": "MVP - Software challenge",
        "descriptor": "Be fluent in written communications with the ability to articulate complex issues, selecting an appropriate structure and with appropriate tone, style and language."
    },
    {
        "name": "4.10.1.4",
        "type": "Security",
        "group": "MVP - Software challenge",
        "descriptor": "Design and develop training materials and plan end user training following software deployment, include consideration of human and technical security requirements."
    },
    {
        "name": "4.10.1.5",
        "type": "Technical",
        "group": "MVP - Software challenge",
        "descriptor": "Create and specify a software design for a medium-size software product using a software requirement specification, an accepted program design methodology (e.g., structured or object-oriented), and appropriate design notation."
    },
    {
        "name": "4.10.2.1",
        "type": "Agile",
        "group": "Lifecycle - Software challenge",
        "descriptor": "Formulate a project scope, timescale, aims and objectives and be able to construct a project specification and plan for a multi-threaded project, including resources and budget."
    },
    {
        "name": "4.10.2.2",
        "type": "Business",
        "group": "Lifecycle - Software challenge",
        "descriptor": "Create a market research report for an industry."
    },
    {
        "name": "4.10.2.3",
        "type": "Employability",
        "group": "Lifecycle - Software challenge",
        "descriptor": "Evaluate standard selling, questioning, negotiating and closing techniques in a range of interactions and engagements, for both internal and external stakeholders."
    },
    {
        "name": "4.10.2.4",
        "type": "Security",
        "group": "Lifecycle - Software challenge",
        "descriptor": "Review common pitfalls and mitigations in common quality assurance techniques."
    },
    {
        "name": "4.10.2.5",
        "type": "Technical",
        "group": "Lifecycle - Software challenge",
        "descriptor": "Design, evaluate and implement a test plan for a medium-size code segment or larger."
    },
    {
        "name": "4.10.3.1",
        "type": "Agile",
        "group": "Release - Software challenge",
        "descriptor": "Manage a project including identifying, evaluating and resolving deviations and the management of the problems and escalation processes."
    },
    {
        "name": "4.10.3.2",
        "type": "Business",
        "group": "Release - Software challenge",
        "descriptor": "Create a marketing plan for communicating the product to the relevant audience."
    },
    {
        "name": "4.10.3.3",
        "type": "Employability",
        "group": "Release - Software challenge",
        "descriptor": "Evaluate the preferences, motivations, strengths and limitations of other people and apply these insights to work more effectively with and to motivate others."
    },
    {
        "name": "4.10.3.4",
        "type": "Security",
        "group": "Release - Software challenge",
        "descriptor": "Critically evaluate the properties of good software design including the nature and the role of associated documentation, appreciating that design increasingly covers the use of existing code and 3rd party elements that may be an alternative to development of code from scratch through make/buy decisions. Assess these properties in a security context and impact on the threat model."
    },
    {
        "name": "4.10.3.5",
        "type": "Technical",
        "group": "Release - Software challenge",
        "descriptor": "Compose a software configuration and release pipeline that complies with industry best practices."
    },
    {
        "name": "4.11.1.1",
        "type": "Agile",
        "group": "Long tail of the software lifecycle",
        "descriptor": "Explain the industry standards in software project management including PRINCE2, ISO 10006, ISO 12207 along with the SEI's CMMI methods, and integrate relevant standards into a project."
    },
    {
        "name": "4.11.1.2",
        "type": "Business",
        "group": "Long tail of the software lifecycle",
        "descriptor": "Use the principles of business transformation to create a recommendations report by being able to decompose and abstract a non-obvious business problem, structure it, collect relevant information, consider options and make recommendations."
    },
    {
        "name": "4.11.1.3",
        "type": "Employability",
        "group": "Long tail of the software lifecycle",
        "descriptor": "Evaluate how personal profiling tools predict team performance in a non-trivial project."
    },
    {
        "name": "4.11.1.4",
        "type": "Security",
        "group": "Long tail of the software lifecycle",
        "descriptor": "Research and evaluate the current threat landscape, trends and their significance."
    },
    {
        "name": "4.11.1.5",
        "type": "Technical",
        "group": "Long tail of the software lifecycle",
        "descriptor": "Profile the performance of existing code and be able to refactor where appropriate, considering improvements to efficiency, scalability, maintainability and extensibility."
    },
    {
        "name": "4.11.2.1",
        "type": "Agile",
        "group": "Enterprise transformation",
        "descriptor": "Plan and create, including contingencies, a risk register (RAID log) to communicate risks and issues."
    },
    {
        "name": "4.11.2.2",
        "type": "Business",
        "group": "Enterprise transformation",
        "descriptor": "Evaluate an employer's business objectives and strategy and its position in the market and how an employer adds value to its clients through its services and/or products it provides."
    },
    {
        "name": "4.11.2.3",
        "type": "Employability",
        "group": "Enterprise transformation",
        "descriptor": "Design and apply performance evaluation tools (including 360-degree feedback) for evaluating team performance."
    },
    {
        "name": "4.11.2.4",
        "type": "Security",
        "group": "Enterprise transformation",
        "descriptor": "Investigate the components and steps of a Business Continuity Plan/Disaster Recovery Plan (BCP/DRP) (e.g. ISO/ IEC 27031) and explain how to support BCP/ DRP measurement of recovery – Recovery Time Objective (RTO)/Recovery Point Objective (RPO)/ Maximum Tolerable Downtime (MTD)."
    },
    {
        "name": "4.11.2.5",
        "type": "Technical",
        "group": "Enterprise transformation",
        "descriptor": "Assess existing legacy software, and review the source code if available, appreciating that part or all of the system may be implemented using an obsolete programming language."
    },
    {
        "name": "4.12.1.1",
        "type": "Agile",
        "group": "Applied research",
        "descriptor": "Create a quality assurance plan and evaluate project deliverables against quality requirements."
    },
    {
        "name": "4.12.1.2",
        "type": "Business",
        "group": "Applied research",
        "descriptor": "Critically evaluate the impact of advanced algorithms in developing new capabilities for businesses, assess the impact on society, and identify future trends."
    },
    {
        "name": "4.12.1.3",
        "type": "Employability",
        "group": "Applied research",
        "descriptor": "Manage and assess different, competing interests within and outside the organisation with excellent negotiation skills."
    },
    {
        "name": "4.12.1.4",
        "type": "Security",
        "group": "Applied research",
        "descriptor": "Establish the impact on security of advanced algorithms for use in applications, and evaluate important social and ethical considerations in how these types of algorithms are being deployed by business and government."
    },
    {
        "name": "4.12.1.5",
        "type": "Technical",
        "group": "Applied research",
        "descriptor": "Develop detailed knowledge and understanding in one or more specialisms, some of which is informed by, or at the forefront of, the subject."
    }
]
