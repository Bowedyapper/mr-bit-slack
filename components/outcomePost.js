/**
 * @description This file builds and returns the message structure using the slack-block-builder package
 * @author Jason Hewitt
 * @version 1.0.0 
*/


import { Message, Blocks, Elements, setIfTruthy, omitIfTruthy } from 'slack-block-builder';


/**
 *
 * @description This function constructs the buttons based on the amount of outcomes we have in the array passed in by outcomeMessae.js
 * @param {Array} outcomeArray The array containing each outcome as an object
 * @return {Array} 
 */
const buttonBuilder = (outcomeArray) => {
    const buttonArray = [];
    for (const outcome in outcomeArray) {
        buttonArray.push(
            Elements.Button({
                text: `LO: ${outcomeArray[outcome]}`,
                actionId: `openLoModal -${outcome}`,
                value: outcomeArray[outcome]
            }).confirm()
        );
    }
    return buttonArray;
};


/**
 *
 * @description This function builds the post that will be sent in reply to a user using the LO syntax e.g. LO:1.1.1.1
 * @param {Array} outcomeArray The array of outcomes found by outcomeFinder from outcomeMessage
 * @param {Object} message
 * @return {Object} 
 */
const outcomePost = (outcomeArray, message) => {
    return Message()
        .blocks(
            omitIfTruthy(message.channel_type === 'im',
                Blocks.Section({
                    text: `Here's the LO's I found from <@${message.user}>'s post`
                })
            ),
            setIfTruthy(message.channel_type === 'im',
                Blocks.Section({
                    text: `Here's the LO's i found for you`
                })
            ),
            Blocks.Actions()
                .elements(
                    buttonBuilder(outcomeArray)
                )
        )
        .asUser()
        .getBlocks();
};


export default outcomePost;