/** 
 * 
 * @description This file builds the modal for displaying outcomes for both the button and slash command
 * @author Jason Hewitt
 * @version 1.0.0
 */


import { Modal, Blocks, setIfTruthy } from 'slack-block-builder';


/**
 * 
 * @param {Object} outcome The outcome as an object we want to display in the modal
 * @returns {Object} The built object that can now be sent to slack
 */
const outcomeModal = (outcome) => {
    return Modal({ title: `LO: ${outcome.name}` })
        .blocks(
            Blocks.Section()
                .fields([`*${outcome.type}*`, `*${outcome.group}*`]),
            Blocks.Divider(),
            Blocks.Section({
                text: ':page_with_curl: *Descriptor*'
            }),
            Blocks.Section({
                text: outcome.descriptor
            }),
            setIfTruthy(outcome.explanation && outcome.example, [
                Blocks.Section({
                    text: ':speech_balloon: *Explanation*'
                }),
                Blocks.Section({
                    text: outcome.explanation
                }),
                Blocks.Section({
                    text: ':woman-tipping-hand: *Example*'
                }),
                Blocks.Section({
                    text: outcome.example
                })
            ]))
        .buildToObject();
};


export default outcomeModal;