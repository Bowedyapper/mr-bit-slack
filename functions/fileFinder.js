/**
 * 
 * @description This file exports the file finder function which can be used to recursively find files
 * @author Jason Hewitt
 * @version 1.0.0
 */


import { readdirSync, statSync } from 'fs';


/**
 * 
 * @description Recursively finds files in a specified path
 * @param {string} directoryPath The directory that we would like to scan
 * @param {Array} arrayOfFiles An optional array we can specify that the files will be pushed too
 * @returns {Array} Returns an array of files that we can iterate over and load
 */
const fileFinder = (directoryPath, arrayOfFiles) => {
    const files = readdirSync(directoryPath);
    arrayOfFiles = arrayOfFiles || [];
    files.forEach(function (file) {
        if (file.split('')[0] === '-') return;
        if (statSync(`${directoryPath}/${file}`).isDirectory()) {
            arrayOfFiles = fileFinder(`${directoryPath}/${file}`, arrayOfFiles);
        } else {
            arrayOfFiles.push(`${directoryPath}/${file}`);
        }
    })
    return arrayOfFiles;
}


export default fileFinder