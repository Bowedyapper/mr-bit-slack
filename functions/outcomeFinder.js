/**- 
 * 
 * @description This module exports a function that can be called to search through the learning outcomes file for a match
 * @author Jason Hewitt
 * @version 1.0.0
*/


import outcomes from '../learningOutcomes.js';


/**
 * 
 * @description This function uses the .find() function for objects that allows us to search for our query
 * @param {string} searchQuery The string we want to search for e.g 1.1.1.1
 * @returns {Object | Boolean} Returns the entire learning outcome object for the specific outcome or false if not found
*/
const outcomeFinder = (searchQuery) => {
    const outcome = outcomes.find(element => element.name === searchQuery);
    if (!outcome) return false;
    return outcome;
}


export default outcomeFinder;