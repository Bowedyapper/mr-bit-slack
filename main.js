/** 
 * 
 * @description This is the main server file for Mr Bit
 * @author Jason Hewitt
 * @version 1.0.0
*/


import slack from "@slack/bolt";
import fileFinder from "./functions/fileFinder.js";
import dotenv from 'dotenv';


if (process.env.NODE_ENV !== 'production') {
    dotenv.config();
}


const { App } = slack;
const port = process.env.PORT || 3000;
const handlers = fileFinder('./handlers');
const app = new App({
    token: process.env.SLACK_BOT_TOKEN,
    signingSecret: process.env.SLACK_SIGNING_SECRET,
    socketMode: true,
    appToken: process.env.APP_TOKEN
});


handlers.forEach(async element => {
    try {
        (await import(element)).default(app);
        console.log(`Loaded ${element}`);
    } catch (error) {
        console.log(`Could not import ${element}`);
        console.log(error);
    }
});


(async () => {
    await app.start(port);
    console.log('Mr Bit for Slack is running on port', port);
})();
