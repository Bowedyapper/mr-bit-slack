module.exports = {
  "parser": "@babel/eslint-parser",
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaVersion": 8,
    "sourceType": "module",
    "allowImportExportEverywhere": true,
    "ecmaFeatures": {
      "jsx": false,
      "modules": true,
      "experimentalObjectRestSpread": true
    },
        
    "requireConfigFile": false,

  },
  "env": {
    "node": true,
    "mocha": true,
    "es6": true
  },
  "rules": {
    "indent": [
      "error",
      4
    ],
    "handle-callback-err": [
      "error"
    ]
  },
  "settings": {
    'import/resolver': {
      "node": {
        "extensions": ['.js', '.jsx', '.d.ts', '.ts', '.tsx'],
        "moduleDirectory": ['node_modules', 'functions', 'handlers'],
      },
    },
  },
};
