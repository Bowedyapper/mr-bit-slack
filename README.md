

# Mr Bit for Slack <img src="https://json.scot/projects/mrbit/img/mrbitslack.png" width="28px" height="auto">

>This is a student created bot that aims to improve productivity within the cohort and perhaps generate some learning outcomes too!



## Get Started

If you would like to clone this bot and use within your own workspace for testing, you first need to create a .env file with your slack tokens like:
```.env
SLACK_BOT_TOKEN=<BOT TOKEN>
SLACK_SIGNING_SECRET=<SECRET>
APP_TOKEN=<APP TOKEN>
```

I would go over how to get these tokens and setup a bot, but this readme would become a book, so I think its best you start [Here](https://slack.com/intl/en-gb/help/articles/115005265703-Create-a-bot-for-your-workspace)

Once you get the keys you need you add them to your .env and then run ```npm install``` to install all of the dependencies for this to run

Then simply run ```npm run dev``` to start the server 😁

## How to contribute
You can start by creating a branch, the master or main branch is protected so it won't allow direct commits, this is because when new changes are pushed a pipeline will run and deploy the new iteration immediately, so I think its safer to be able to enforce proper merge / pull requests to stop things breaking and/or malicious code being added :S

<small><small><small>Like Jordan stealing trying to steal my API keys 😂</small></small></small>

### Structure
```Root
 ┣ 📂components
 ┃ ┣ 📜someComponent.js
 ┣ 📂functions
 ┃ ┣ 📜someReusableFunction.js
 ┣ 📂handlers
 ┃ ┣ 📂actions
 ┃ ┃ ┗ 📜actionHandler.js
 ┃ ┣ 📂commands
 ┃ ┃ ┗ 📜commandHander.js
 ┃ ┗ 📂message
 ┃ ┃ ┗ 📜messageHander.js
 ┣ 📜.eslintrc.cjs
 ┣ 📜.gitignore
 ┣ 📜.gitlab-ci.yml
 ┣ 📜Dockerfile
 ┣ 📜jsconfig.json
 ┣ 📜jsdoc.conf.json
 ┣ 📜learningOutcomes.js
 ┣ 📜main.js
 ┣ 📜package-lock.json
 ┣ 📜package.json
 ┗ 📜README.md
```
This structure was chosen as it seemed the most visibly logical way for me to be able to sort out the different things a slack bot can listen for, actions being for button presses, commands being for /lo and messages for message events
### Packages
These are the current packages in use by this project to make things a little easier and tidier
* Bolt by Slack - [https://slack.dev/bolt-js](https://slack.dev/bolt-js)
* Slack Block Builder - [https://www.blockbuilder.dev](https://www.blockbuilder.dev)
* Dotenv - [https://www.npmjs.com/package/dotenv](https://www.npmjs.com/package/dotenv)
* JSDoc - [https://github.com/jsdoc/jsdoc](https://github.com/jsdoc/jsdoc)
* ESLint - [https://www.npmjs.com/package/eslint](https://www.npmjs.com/package/eslint)
*  Babel ESLint Parser - [https://www.npmjs.com/package/@babel/eslint-parser](https://www.npmjs.com/package/@babel/eslint-parser)

## Documentation
You can find the documentation for this app at [https://json.scot/projects/mrbit/docs](https://json.scot/projects/mrbit/docs)

If you would like to add to these documents, you can do so by adding JSDoc style comments to your code and you can test the output by installing the JSDoc NPM package and running 

```npm
npm run document
```

This will create a folder called /documentation in the project directory with plain HTML files that you can open locally in your browser