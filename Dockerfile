FROM node:alpine as build
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json
COPY . /usr/src/app
RUN npm install && npm install pm2;
CMD [ "pm2-runtime", "main.js"]
