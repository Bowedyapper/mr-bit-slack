/** 
 * 
 * @description This file creates the event listener for messages that contain the pattern for learning outcome requests
 * @author Jason Hewitt
 * @version 1.0.1
*/


import outcomePost from "../../components/outcomePost.js";
import outcomeFinder from "../../functions/outcomeFinder.js";
const outcomeDetectionRegex = /^lo:|LO:|lo:|\[LO:/gmi;
const numberExtractionRegex = /(?:lo:\s?)(\d\.\d\d?\.\d\.\d)/gmi;


/**
 * 
 * @description This function uses the regex to find the specific numbers relating to the learning outcome and puts them in an array and returns that array
 * @param {string} userMessage The text string from the users message
 * @returns {Promise}
 */
const findLearningOutcomesInMessage = (userMessage) => {
    return new Promise((resolve, reject) => {
        const matches = Array.from(userMessage.matchAll(numberExtractionRegex), match => match[1]);
        const m = matches.filter(item => outcomeFinder(item))
        if (m.length === 0) return reject(`No match found in: ${userMessage}`);
        resolve(m);
    });
};
/* This was the original function but couldn't decide which was better in terms of readability,
 *  for me the best option was the shorter one but I leave the old one here just incase
/*
const findLearningOutcomesInMessage = (userMessage) => {
    return new Promise((resolve, reject) => {
        const matches = userMessage.matchAll(numberExtractionRegex)
        if (matches.length > 0) return reject();
        const outcomeArray = []
        for (const match of matches) {
            outcomeArray.push(match[1])
        }
        resolve(outcomeArray)
    });
}*/


/**
 * 
 * @description This function sets up the event listener for incoming messages 
 * @param {Object} app The app object which was defined in the main file
 * @listens message Listens for a message matching the regex
 */
const outcomeEventListener = (app) => {
    app.message(outcomeDetectionRegex, async ({ message, say }) => {
        const userMessage = message.text
        findLearningOutcomesInMessage(userMessage)
            .then(async (outcomeArray) => {
                try {
                    console.log(outcomeArray)
                    await say({
                        text: 'Here is the LO\'s I found',
                        blocks: outcomePost(outcomeArray, message),
                        thread_ts: message.thread_ts
                    });
                } catch (error) {
                    console.log(error);
                    await say('I am sorry, something went very wrong :|');
                }
            })
            .catch(error => console.log(error));
    });
};


export default outcomeEventListener;