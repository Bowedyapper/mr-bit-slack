/**
 * 
 * @description This file sets the event listener for the /lo command from slack
 * @author Jason Hewitt
 * @version 1.0.0
*/


import outcomeModal from '../../components/outcomeModal.js';
import outcomeFinder from '../../functions/outcomeFinder.js';


/**
 * 
 * @description This function sets up the event listener
 * @param {Object} app The app object which was defined in the main file
 * @listens /lo Listens for a user executing the /lo command from slack
 */
const learningOutcomeEventListener = (app) => {
    app.command('/lo', async ({ ack, body, client }) => {
        await ack();
        const outcome = await outcomeFinder(body.text);
        if(!outcome) return client.chat.postEphemeral({
            channel:body.channel_id,
            user: body.user_id,
            text:'Sorry, that learning outcome does not exist :('
        });
        await client.views.open({
            trigger_id: body.trigger_id,
            view: outcomeModal(outcome)
        });
    });
};


export default learningOutcomeEventListener;