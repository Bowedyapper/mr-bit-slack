/** 
 * 
 * @description This file creates the event listener for button clicks and slash commands to open a modal
 * @author Jason Hewitt
 * @version 1.0.0
*/


import outcomeFinder from "../../functions/outcomeFinder.js";
import outcomeModal from "../../components/outcomeModal.js";


/**
 * 
 * @description This function sets up the event listener for requests to open a modal / view
 * @param {Object} app The app object which was defined in the main file
 * @listens command Listens for a command to open a modal
 */
const openOutcomeModal = (app) => {
    app.action(/openLoModal -\d\d?/, async ({ack, body, client})=>{
        await ack();
        const outcome = outcomeFinder(body.actions[0].value);
        if(!outcome) return;
        await client.views.open({
            trigger_id:body.trigger_id,
            view:outcomeModal(outcome)
        });
    });
};


export default openOutcomeModal;